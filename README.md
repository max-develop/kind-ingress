# Traefik Ingress on Kind

## Create a Kind Cluster

To enable ingress support in Kind you have to run following command to allow ingresses in your cluster

```sh
cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
        authorization-mode: "AlwaysAllow"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
EOF
```

## Deploy Traefik

Create the traefik namespace.
```sh
kubectl create ns traefik
```

You can use the given configuration to deploy traefik.
Run following command the deploy a traefik ingress and the traefik dashboard.

```sh
kubectl apply -f traefik/
```

You can reach the dashboard under `http://localhost`

## Deploy the demo app

Create the app namespace.

```sh
kubectl create ns app
```

Deploy the demo application by running:

```sh
kubectl apply -f demo-app
```

You can reach the app here: `http://app.localhost`
